/* Generated automatically by the configure script */

/* ncurses library */
/*#define HAVE_NCURSES 1*/

/* ncurses 4.2 or better have define_key (In Linux) */
/*#define HAVE_DEFINE_KEY 1*/

/* The X11 keysyms are there */
/*#define HAVE_KEYSYMS 1*/

/* X11 library and headers */
/*#define HAVE_X11 1*/

/* International support with gettext */
#define HAVE_INTL_SUPPORT 1

/* GPM mouse support */
/*#define HAVE_GPM 1*/

/* out/in functions defined by glibc */
/*#define HAVE_OUTB_IN_SYS 1*/

/* Use stream replacements */
#define HAVE_SSC 1

/* Byte order for this machine */
/*#define TV_BIG_ENDIAN 1*/

/* Linux implementation of POSIX threads */
/*#define HAVE_LINUX_PTHREAD 1*/

/* Memcpy doesn't support overlaps */
/*#define HAVE_UNSAFE_MEMCPY 1*/

#define TVOS_DOS
#define TVOSf_
#define TVCPU_x86
#define TVComp_GCC
#define TVCompf_djgpp

#define TVCONFIG_RHIDE_STDINC "$(DJDIR)/include $(DJDIR)/lang/cxx $(DJDIR)/lib/gcc-lib g:/dj/lib/gcc/djgpp/3.43"
#define TVCONFIG_TVSRC "../../include g:/dj/contrib/tvision/include g:/dj/include/rhtvision"
#define TVCONFIG_RHIDE_OS_LIBS "stdcxx tvfintl"
#define TVCONFIG_TVOBJ "../../makes g:/dj/contrib/tvision/makes g:/dj/lib  ../../intl/dummy"
#define TVCONFIG_STDCPP_LIB "-lstdcxx"
#define TVCONFIG_SHARED_CODE_OPTION "-fPIC"
#define TVCONFIG_RHIDE_LDFLAGS ""
#define TVCONFIG_LIB_VER "2.0.3"
#define TVCONFIG_LIB_VER_MAJOR "2"
#define TVCONFIG_EXTRA_INCLUDE_DIRS ""
#define TVCONFIG_CFLAGS   "-O2 -Wall -Werror -gstabs+3"
#define TVCONFIG_CXXFLAGS "-O2 -Wall -Werror -gstabs+3 -fno-exceptions -fno-rtti"
#define TVCONFIG_REF_DIR  "g:/dj/contrib/tvision"

